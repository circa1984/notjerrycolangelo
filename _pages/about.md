---
layout: page
title: A Propos
permalink: /about
comments: false
image: assets/images/about.png
imageshadow: true
---

Ce blog est né à l'été 2022.
La Free Agency battait son plein et clairement c'était un été nerveux du coté de l'Arizona. L'équipe sortait d'une saison régulière incroyable, avec le record de victoires, Monty Williams chopait le COY, mais on allait être témoin de la chute catastrophique du collectif et des individualités des Suns.
On se fait ceuillir en 7 par des Mavs accrocheurs, apres, hargneux.
Là dessus, on doit faire face à Ayton nouveau RFA (Restricted Free Agent), on regarde notre conccu directe se renforcer, et nous ?
Nous on attend Kevin Durant.
Ce grand échalat qui annonce, juste avant que Juillet ne s'ouvre, qu'il veut rejoindre son pote D.Book à Pheonix, alors qu'il lui reste 4ans en salaire max à taper chez les Nets. Une bombe.

Ce blog nait dans ma tête juste avant cette bombe. J'écris alors un article que je propose à la team de Trashtalk, mais ils ne prennent pas de rédacteurs extérieurs à ce moment là. Leur retour sur l'article lui-même est tout de même positif (merci encore la rédac TT !), alors je me dis "frérot, tu vas monter ton endroit et ciao bambino" - avec un très mauvais accent italien.

Quelques jours de code plus tard, le blog est en prod (Jekyll sur Gitlab Page pour celles et ceux qui apprecieront ce genre d'info technique).

On est là, on écrit sur cette équipe qu'on aime, et puis qu'est-ce que tu veux de plus finalement ?
