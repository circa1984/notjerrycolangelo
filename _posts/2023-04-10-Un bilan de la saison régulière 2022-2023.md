---
layout: post
title: "Un bilan de la saison régulière 2022-2023"
author: gregoire
categories: [ Suns, saison ]
tags: [ Saison régulière, Devin Booker, Mikal Bridges, Kevin Durant, Mat Ishbia, Trades ]
image: 'assets/images/18.png'
---

En 2021-22, les Suns finissaient leur saison régulière avec un bilan de 64-18, 1er de la conférence Ouest. En 2022-23, 4ème d'un Ouest redoutable et serré comme mes pantalons d'avant le mariage, les Suns affichent un bilan de 45-37 et affronteront les Los Angeles Clippers au premier tour des Playoffs.
Une saison n'est vraiment prompte à être analysée par la perspective des échecs et des réussites que lorsque les Playoffs sont terminés et que le champion est sacré. Mais la saison régulière de Phoenix a été si riche, si intense, qu'il convient à mes yeux de revenir un poil dessus à l'aube de la dernière ligne droite dans la course au titre.

#### Jean Claude Gros Doss

On va pas se mentir, le très gros dossier de cette saison régulière, c'est le changement de propriétaire à la tête de la franchise (et de celle du Mercury en WNBA). On parlera de l'arrivée de KD plus loin dans cet article, et il ne s'agit pas de limiter l'impact de l'arrivée de l'ailier all time chez les cactus. Non il s'agit juste de mesurer calmement et sereinement l'impact que ce changement de propriétaire a eu, a encore, et aura encore un bon moment, sur la franchise de l'Arizona.

Son prédécesseur était une ordure. Heureusement, l'équipe, par son jeu, ses résultats et son leadership exemplaire, avait su détourner la fanbase du gros de cette affaire, permettant de la voir se régler dans son coin. Mat Ishbia prenait donc la tête des deux franchises basket de la région à quelques jours de la trade deadline, début Février. Ce que ca signifie pour ces structures est gros : la fin d'une culture d'entreprise toxique et néfaste. Pour de nombreuses et de nombreux ancien.nes employé.es, des dédomagements. Pour les gens qui bossent encore dans ces franchises, la garantie d'un nouveau souffle, et d'une nouvelle méthode dans leur quotidien.

Ce n'est pas rien, que de laver cette honte. Ce n'est pas anodin dans une franchise, que d'être enfin en mesure de savoir que les gens qui bossent aussi à notre bonheur et aux résultats de l'équipe, quelque soit leur job au quotidien, aient des conditions de travail qui s'améliorent mécaniquement avec ce changement de propriétaire. Un gros dossier de clos, grâce à cette vente. C'était pour ma part un des plus gros objectif que je voulais voir accompli cette saison dans la franchise de Phoenix, et je suis très heureux, de fait, que ce soit chose faite.

#### D'Octobre à Février

Bon sang que cette saison a été difficile. La cinquantaine de matchs avant la trade deadline de février a été comme une série de montagnes russes. Il fallait s'accrocher. Fort. S'aggripper même, tant on est passé du très haut au très bas, tant on a manqué de repères, de lumière, et tant on a collectionné les galères. Un enfer hivernal après deux premiers mois absolument divins.

C'est simple, on attaque la sason avec, pour seul point noir, la relation Crowder / FO. L'ailier, ancien titulaire au poste 4 chez nous, a vu sa place dans le 5 être prise par Cam J. Refus net et précis du vieux briscard qui, d'un comme un accord avec le FO, s'est coupé du reste de l'équipe en attendant un trade quelconque.

Mis à part cette histoire d'égo, le début de saison est stratosphérique. Les Suns dominent une conférence Ouest devant Denver et Memphis, à l'aise dans le rôle de contenders sûrs qu'ils ont sur les épaules depuis la saison 2020-2021. Booker joue un basketball de très très haut niveau. Il tutoie Jokic, Embiid et Tatum dans la course au MVP lancée très tôt cette saison, et chope un Player of the Month. Monty brigue aussi une récompense individuelle, et les Suns ne s'arrêtent pas là puisqu'Ayton va se chercher un Player of the Week. Au milieu de ça, Cam J est blessé, ce qui voit Torrey Craig être en missio dans le 5.

Mais les blessures ne s'arrêtent pas là. Tour à tour, Paul, Booker, Payne, Ayton... C'est quasiment l'intégralité de l'effectif des Suns qui va voir l'infirmerie. L'équipe jouera en tout et pour tout une quinzaine de matchs avec un 5 complet. Ridicule dans cette course sanguinaire qu'est la conférence Ouest.

Au creux de l'hiver, les Suns affichent des line-ups Sabel Lee, Damion Lee, Mikal Bridges, Torrey Craig, Jock Landale, pour affronter des cadors et prendre des roustes pas possibles. De premiers à l'Ouest, les cactus vont chuter jusqu'aux Playin. Mikal ne va rater aucun match. Aucun.

C'est sur ses épaules qu'en Janvier et Février, Monty Williams va poser toutes les responsabilités offensives. Ancienne option 3B sinon 4, Kal va se retrouver à la pointe du jeu des Suns, à conduire une équipe digne du meilleur tanking imaginable. Dur de trouver ses marques, dur de trouver les filets, au début en tout cas. Et puis déclic. Mikal va enchainer les excellentes perfs et passer de nouveaux caps. 20ppg de moyenne avec les Suns sur sa grosse dizaine de dernières matchs pour nous. On avait cette seconde option offensive qu'on cherchait tant depuis l'été. Elle était là.

Mais Ishbia, le FO et Monty Williams vont imaginer autre chose pour l'avenir de la saison et de la franchise. Février arrive, la trade deadline avec, et Kevin Durant aussi. La contre-partie vous la connaissez autant que moi : Mikal, Cam J, quelques tours de draft filent en direction de Brooklyn, y réussir une fin de saison absolument énorme, qui verra les Nets (désormais franchise soeur) se qualifier pour les Playoffs.

#### De Février à Avril

C'est un tel chamboulement dans la vie des fans des Suns, que deux sentiments émergent et dominent la fanbase un peu partout dans le monde. On va pas revenir sur ce trade, ya des threads et des articles entiers à ce propos. Mais voir la franchise se diviser dans l'émotion, entre celles et ceux qui regrettent toujours Mikal  et Cam J (dont je fais partie) et les autres qui sont émerveillé.es de l'arrivée de Durant... C'est impressionnant.

Parlons-en tiens, de KD. Son apport aux Suns a été extraordinaire... sur seulement 8 matchs.
Alors certes, 8 games c'est pas bezef pour créer de l'alchimie, de la routine, des automatismes, pour réduire les risques et augmenter les chances. Certes. Mais bizarement, ces 8 matchs sont aussi 8 victoires. Des wins avec la manière, des wins propres.

Des matchs où on a vu Durant et Booker s'entendrent à merveille, et se relayer quand l'un ou l'autre n'est pas dans sa meilleure soirée. 8 matchs où on a vu Chris Paul plus à l'aise à l'idée d'embrasser ce rôle en catch and shoot que Monty et son playbook ont dessiné pour lui depuis l'été 2022. 8 matchs où on a aussi pu se rendre compte que, même sansaugmenter son rendement statistique, la menace Deandre Ayton au milieu du duo Book-Durant offre des solutions jamais vues dans le jeu modernes des Suns.

Quand KD n'a pas été là, après s'être bêtement fait une cheville à l'échauffement avant-match, l'équipe a peiné. Rien de curieux, rien de surprenant. La profondeur de jeu est moindre lorsqu'on enlève un tel joueur des rotations... Mais cette saison se termine de la bonne façon. Après avoir été vertigineuse et hautement vomitive pour chaque fan des Suns, les cactus pointent donc 4ème de cet Ouest sanguinaire, et semblent craints de bien des fanbases qui leur crachaient pourtant dessus avant le début de saison...

#### Shoutouts

On peut pas boucler cette régulière sans quelques shoutouts à nos role players. J'ai trouvé cette équipe hivernale, faite de briques et de Jock, attachante par sa motivation et sa determination à ne rien lâcher. Y'a des mecs types qui auraient crevé sur le parquet s'i ca avait pu nous rapporter une win lorsqu'on enchainait les défaites. Je pense au match de Torrey Craig contre un Zion Williamson titanesque. Je pense à Damion Lee et son adresse prodigieuse jusqu'au ASG. Je pense à l'énergie de Jock Landale et à la défense de Bismack Biyombo. Je pense à Ish qui va se chercher un contrat garanti à la sueur et aux corners. Je pense aussi à Saben Lee, dont la propreté et la sobriété ont aussi payé. Je pense enfin à Josh Okogie, dont le boulot péhnoménal des deux cotés du terrain à tant payé qu'il est starter aujourd'hui, à la pointe de la défense d'un des plus gros prétendants au titre.

Comment ne pas s'attacher à ses gars ? Bien sûr ils sont pas parfaits. Bien sûr qu'on a perdu X matchs parce qu'ils ont fait des erreurs que les titulaires ne font pas. Bien sûr que le bilan des Suns a plongé quand 75% du 5 majeur de Monty était blessé et que ces gars ont pris la relève. Mais voir ces mecs se défoncer tout l'hiver pour éviter de perdre, et recommencer le lendemain, ca m'a plu. Ca m'a plu de savoir qu'ils vont finir la saison et aller en playoffs donner de précieuses minutes gorgées de l'expérience accumulée cet hiver et soucieux de ne plus rien perdre, de ne plus se laisser botter le cul par X ou Y.

On attaque les playoffs morts de faim, l'humiliation des Playoffs de l'an dernier n'est pas guérie parce que les Mavs ne sont même pas en PO cette année. Non, on s'en fout de ça. L'Arizona toute entier s'en fout de ça. Cette équipe va laver ce sale souvenir sur le parquet de chaque match de ces playoffs à venir. Et on a hâte de regarder ça et de la soutenir.

Lets GO !