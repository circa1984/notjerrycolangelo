---
layout: post
title:  "Bilan de la première séquence"
author: gregoire
categories: [ Saison, Suns ]
tags: [ Bilan, Saison, Monty, Booker, Ayton ]
image: 'assets/images/13.png'
---

Meilleur bilan de l'Ouest. Troisième meilleur bilan de la league derrière les imprenables de Milwaukee et les excellents Cavs. 7 victoires pour deux défaites.

Voilà d'où les Suns s'appretent à entamer leur premier voyage vers l'Est après 9 matches à l'Ouest pour démarrer la saison. Le calendrier avait en effet placé 9 matches d'affilé contre des équipes de la conférence Ouest sur le début de saison des Suns. Une conférence où la conccurence devrait être intense en fin de saison pour se distribuer les avantages de playoffs : adversaires moins bien classé, avantage du terrain, play-in...

Il fallait être costaud sur ces 9 matches. Phoenix l'a été. Pas parfait, pas ultra-dominant, on pourra trouver à redire. Mais les joueurs et le staff ont fait le job. Revenons d'ailleurs là dessus.

#### Jean Claude 5 majeurs de cracks

C'était essentiel. Il fallait tirer avant tout les leçons de nos playoffs l'an dernier. On savait qu'on retrouvait la même équipe ou presque pour attaquer cette nouvelle saison. On savait donc quels étaient les forces qui nous avaient permis d'éclater le record de wins en régulière. Mais on savait aussi qu'il fallait peaufiner un playbook trop heliocentré sur les qualités de Paul, playbook qui permettait à des adversaires intelligents de nous tuer en ciblant notre meneur.

L'ouverture de cette saison, sur ces 9 matches à l'Ouest, c'est la preuve par le jeu que ces ajustements ont été mis en place de main de maître, non seulement pas le boss lui-même, Monty Williams, mais aussi par un Chris Paul qui a su métamorphoser son jeu. Il n'est pas moins présent en attaque. Il n'est pas moins dangereux. Il n'est pas moins précieux. Il est juste moins central dans nos systèmes, et notre dépendance à son rendement et à son état de santé physique ou mentale est minime. C'est une excellente chose, et c'est la plus grosse marque de changement dans le playbook de Monty. Merci Chris d'accepter et d'embrasser ce nouveau rôle. Te voir donner la gonfle à Booker après 1 seul dribble, te voir glisser entre les écrans pour te rendre dispo en catch and shoot, ON ADORE.

Paul réajusté, la conséquence directe c'est Book, qui récupère le lead de l'équipe en attaque et qui l'assume plus que jamais. Les épaules ont grandit en un été et il prend enfin la carrure du franchise dont on avait besoin. Devin Booker propose le meilleur +- de la NBA après 9 matches. Il est dangereux, altruiste, versatile, audacieux et commence à pas du tout rigoler en défense. le step-up est grand pour l'équipe. On le sent. Il joue quasiment l'intégralité des premiers QT à chaque match, pénètre, provoque, avec grande grande propreté. Calibre MVP, même si la conccu de Giannis, Luka et Spida ne manque pas d'arguments.

Ayton. Deandre Ayton. Je suis définitivement monté dans le bus Ayton cet été, intrigué et confiant quand je voyais ce jeune pivot dont on connaissait déjà le potentiel, arriver plein d'aigreur et de rancoeur vis à vis des médias. J'imaginais qu'il allait en tirer une force, et qu'il allait se mettre en route pour être le 5 dominant qu'il peut être. Est-ce que c'est prouvé sur 5 matches ? Non. N'abusons pas. D'autant qu'une petite blessure à la cheville est venue perturber son début de saison... Néanmoins, la bonne volonté, la motivation, la disponibilité sur le terrain, l'application, tout ces ingrédients obligatoires y sont. Ils n'y étaient pas forcément les dernières saisons. Là, on découvre un Ayton qui met ces oeufs dans le bon panier, et qui mise sur lui-même. Ca fait du bien, et nul doute que ca donnera des fruits. Encore un peu de patience.

Et puis le duo 3-4 de notre cinq majeur. Cam Johnson et Mikal Bridges. Commencons par Cam J tiens. On vient d'apprendre sa blessure au genou, ménisque pour être exact. Ca nous rappelle Bledsoe, sauf que Bledsoe bon... bref, ne revenons pas sur cette période dark de l'histoire arizonnienne. Cam J out pour quelques mois, opération du ménisque, on te retrouve en Mars si tout roule mon jeune. Tu nous as donné un début de saison marqué par une progression constante. Timide d'abord, peut-être un peu paumé à la fois par les nouveaux systèmes, également par ce nouveau rôle dans le 5. Puis tout est venu tranquillement, jusqu'à ce match somptueux contre les Wolves, avec tes 64% du parking (PTDR). On a hâte de te revoir.

Mikal. Ma vie. Notre vie à toutes et tous qui suivons les Suns. L'âme de l'équipe. Le chouchou. Combien d'observateurs t'avaient mis dans les trades pour KD ?
On a finalement conservé le fabuleux joueur qu'est Mikal Bridges, important dans notre équipe par son incroyable défense, et ses drives finition eurostep dont je raffole personnellement. Complément parfait de Booker, duo parfait avec Cam J, je manque de mots pour décrire à quel point ce joueur est important dans cette équipe.

#### Le Bilan

7-2 donc, après ces 9 premiers matches de la saison régulière et juste avant d'affronter les Sixers chez eux. Il ressemble à quoi ce bilan en termes statistiques ? Eh bien voilà son joli visage :

![bilan statistique d'équipe - séquence 1](https://rallythevalley.fr/assets/images/teamstats-bilan1.png)

Pas mal non ?
Il y a des choses que j'aime particulièrement lire :
- le net rating : on est 1er de la league sur ce plan là, en laissant nos opposants en moyenne à 11 puntos derrière nous.
- les passes : si le ballon circule bien chez nous (3ème moyenne de passe), on est aussi très bon pour le faire moins bien circuler chez nos adversaires par rapport à nous, avec un différentiel de 7 passes.
- les lancers-francs : souvenons-nous la saison dernière, on allait jamais sur cette foutue ligne. Là, on y va non seulement davantage (merci les drives de Book et Mikal, et leu jeu de DA), mais on a surtout une excellente moyenne de 81%.

Et c'est pas tout. On est 3ème meilleure défense de la league, et troisième meilleure attaque. Et ça c'est du très bon. Cela signifie que des deux cotés du terrain, notre équipe a son équilibre, son organisation, et exprime déjà toutes ses qualités. Les efforts sont collectifs, ca sent dans le jeu et dans l'implication des joueurs.

Tiens, parlons en des joueurs. Stats ? Zébardi :

![bilan statistique du roster- séquence 1](https://rallythevalley.fr/assets/images/rosterstats-bilan1.png)

Je sais pas vous, mais la première chose qui me saute aux yeux, c'est que nos les 5 titulaires pointent à plus de 10pts par match. Et de plus près, une stat se dégage : Mikal et DA sont à quasiment 60% d'éfficacité sur ce début de saison, et autour de 15pts par match de moyenne. Cela signifie que la responsabilisation de chaque joueur du 5 en attaque, grâce aux ajustements du playbook de Monty comme discuté plus haut, est non seulement qantitative, mais aussi qualitative.

On ne se contente pas de chercher les joueurs. On insiste pour les trouver quand et là où ils sont à leur top efficacité, là où ils sont le plus suceptibles de faire mal. Limiter les déchets au maximum. (Ce qui expliquerait du coup pourquoi l'équipe semble complètement désinteressée vis à vis du concept de rebond PTDR, mais c'est un autre débat).

Voilà donc en somme comment ce bilan très positif est représenté dans les stats.
Sur le terrain, on voit également des choses.

D'abord un body language collectif beaucoup plus sérieux, beaucoup plus concentré que l'an dernier. On avait eu cet aperçu dès les trois premiers matches, mais c'est confirmé et ca semble être le mood de l'équipe cette année. On s'en plaindra pas. Le banc n'y est pas pour rien. Jock Landale, Landry Shamet et Damion Lee pour ne citer qu'eux. Pas parfaits, évidemment, mais une grosse volonté d'appuyer le boulot des starters, d'appliquer les consignes, et de mouiller le maillot des Suns. On A-DORE.

On voit aussi, en regardant à l'intérieur des matches, que l'équipe cherche et trouve des solutions sans relâche. Exception faite de la deuxième défaite contre Portland et des solutions pour contenir un Jerami Grant de gala, sur cette séquence de 9 matches, l'équipe a su :
- garder la tête hors de l'eau et revenir plusieurs fois dans le match contre les Mavs en ouverture de saison, pour finir par le gagner
- gérer ses avances et ne pas se montrer arrogante
- puiser dans ses ressources profondes pour contrer des systèmes défensifs ou offensifs déjà adaptés au jeu des Suns 2022-2023

Alors certes, on est punit par le clutch de Portland une première fois, puis par Jerami Grant ensuite, mais au global, l'équipe ne s'avoue jamais vaincue. Un vrai soulagement, tant on était au bout du roulz après la série calamiteuse face à Dallas en playoffs l'an dernier.

#### Prochaine séquence

Alors à quoi ressemble la suite de cette saison ?
Jusqu'à fin Novembre, nous allons alterner les confrontations à l'Est et à L'Ouest. Si notre bilan nous place favoris de quasiment tous les matches en question, il faudra tout de même que ces nouveaux Suns gardent la tête froide et le sérieux dont ils ont su faire preuve jusqu'à présent. On pense notamment aux matches contre le Jazz ou encore les Warriors, sans oublier que tout le monde est dangereux sur un match de saison régulière, et que la moindre victoire aura son importance cette année plus que jamais.

Et n'oublions pas : Booker MVP letzzz fucking GOOOOOO