---
layout: post
title: "Deandre, Deandre, Deandre..."
author: gregoire
categories: [ Suns, Roster ]
tags: [ Deandre Ayton, Saison, Trades ]
image: 'assets/images/16.png'
---

Il me parait loin le temps où Deandre Ayton pétait toutes les raquettes ou presque de la league et prenait un Player of The Week mérité. il me parait loin cet été où, vengeur et acerbe, il se tenait froidement devant les journalistes qui avaient enterré sa carrière à Phoenix, et lui prédisait l'avenir d'un pivot titulaire ordinaire quelque part ailleurs en NBA. Il me parait grave loin le temps où, entre cet été, son nouveau contrat, et ce Player of The Week, j'étais heureux de constater que mes pronostics chauds comme la braise sur un DA en 22-11 cette saison  tenaient bon.

Ils sont refroidis les pronos là. Ils ont pris une grèle pas possible sur le museau. Et moi, conducteur patenté du train Ayton, représentant des gens qui croient en lui et qui essaient de lire entre ses lignes nébuleuses, je suis également refroidi. Mes espoirs commencent à avoir le goût d'un sandwich SNCF ouvert hier matin. Et je suis fin gourmet...

#### Jean Claude Ayton

J'ai pas envie de rentrer dans les stats pour cet article. Deandre, c'est autre chose que des stats. C'est presque une discussion philosophique sur les différentes variables qui entrent en jeu dans l'appreciation d'un joueur, de son apport dans les succès et les échecs individuels et collectifs qu'il traverse.

J'ai presque l'impression que DA, c'est un cas à part. Ou alors j'en fais moi-même ce cas à part ou, parce que je lis trop de choses injustes à son propos, mal pesées, sans perspectives, je le préserve d'un monde qui comprendrait pas son apport aux Suns.

Mais les récentes semaines m'ont pas aidé. Voir Booker sur le banc blessé, voir Mikal chercher son jeu de leader offensif d'une équipe décimée (et le trouver avec un brio qui va pas m'aider à arrêter de tweeter 154 fois par jour "Mikal Bridges"), et voir dans le même temps Deandre Ayton semblant n'avoir aucun impact sur les résultats de l'équipe, comme s'il était en pleine AndreDrummondisation... Je revois mes perspectives.

Que reste-t'il de mon amour pour le pivot ?
Il y a des choses qui me paraissent toujours injustes, et puis il y a des fenêtres qui se sont ouvertes à son sujet, dans mon esprit, et qui n'ont pas l'air de vouloir se fermer rapidement.

#### Inutile de répéter les conneries

Je pense que le principal défaut qu'on trouve quant au jugement de Deandre Ayton et de son apport, c'est de ranger le gars au rayon soft. Ca me parait être un raccourci galopant qui finalement ralentit un jugement impartial et fin qu'on devrait avoir sur le joueur. Deandre Ayton n'est pas soft. Il est soyeux, il préfère les hooks, les jumpers, que l'autorité d'un gros dunk sale sur la truffe de n'importe quel pivot. Il préfère sa technique de shoot à son physique. Il choisit. Parce qu'il sait qu'il est capable. Il n'est pas soft par nature, il est conscient de ce qu'il retient et a ses raisons (justifiables ou pas).

En parlant de choix, revenons sur celui qui pour moi le mets dans une sauce monstre, dont il ne doit pas être responsable. (yes, c'est le passage de l'article où je le défends une nouvelle fois).

Ce choix de draft, tout en haut, Ayton ne l'a pas choisi. J'ai toujours trouvé ça chelou de blâmer des mecs qui sont pris en first pick, et les affubler de busts juste parce qu'ils n'ont pas répondu aux attentes qu'un tel pick mets sur leurs épaules. Mais aucun bust n'a décidé d'être un bust. Et aucun joueur ne répondant pas aux attentes d'un tel statut n'a choisi les exigeances qui sont sûr lui. Evidemment Ayton n'est pas un bust, mais son cas de figure n'est pas vraiment différent dans l'approche qu'on a de son cas : y'a toujours des ouin-ouins qui vont venir expliquer que c'est une fraude parce que le gars est first pick mais ne tourne pas en 35-18 tous les soirs... Flemme.

Sur ce sujet, le vrai problème n'est pas DA. C'est le scouting des Suns. Et c'est le cas pour tous les cas de figure de draft où le joueur ne répond pas aux attentes : c'est un problème de scouting. Est-ce que les scouts de Phoenix avaient fait leur taf sur le profil psychologique et motivationnel de Deandre Ayton ? Et si oui, avec quelle profondeur ?

#### La BradleyBealation de Deandre

Plus haut dans cet article, je parle d'une Drummondisation de DA. Mais la comparaison n'est évidemment pas la bonne. Les observateurs les plus sérieux sur les Suns et Ayton SAVENT que le gars est différent en playoffs. Celles et ceux qui écoutent et lisent ses déclarations de conférence de presse SAVENT que Deandre Ayton ne donne pas tout en saison régulière, et que son objectif c'est de tout défoncer en Playoffs.

Seul hic : il faut aller en Playoffs pour tout défoncer en Playoffs. Niveau CE2 de mathématiques.

Cette saison, DA a montré qu'il pouvait passer en mode agressif. Presque comme s'il avait un bouton dans la tête, et que, lorsque l'occasion le recquiert (ou qu'un élement extérieur type Pat Bev intervient), le beast-mode de la fin Novembre revient. Dans ces moments là, DA est Top 5 pivot de la league. Dans les autres, il dégringole et caresse le niveau défensif de Vucevic, avec un impact offensif statistique, certes, mais jamais fatal, jamais clutch, jamais décisif.

Le joueur qui procède de cette façon depuis quelques saisons, c'est Bradely Beal. Type avec un talent fou, qui pourtant semble se préserver pour les PO. La comparaison doit s'arrêter là, parce que Bradley il a pas compris que si sa team va pas en PO, c'est aussi, peut-être hein, c'est aussi parce qu'il choisit ses matchs. Et personne ne souhaite que DA en arrive là.

Deandre, Deandre, Deandre... Frérot... Booker est out, Mikal n'est pas une première option. Tu dois l'être. Quand personne n'est là, tu dois l'être. Tu dois guider cette équipe et arracher des victoires. Cette saison, la conférence Ouest c'est tout sauf des lolz. Chaque victoire est importante. On a besoin de ton beast-mode H24, entrainement compris. On a besoin que tu prennes les responsabilités offensives, et que défensivement, tu élèves ton niveau d'impact. Tu as tout entre les mains pour le faire. tes préférences ne sont pas les besoins de l'équipe. Et malheureusement, tes préféfences ont un poids dans le destin de cette équipe. C'est pour ça que Mikal et Monty te gueulent dessus, c'est pour ça que Book, ton capitaine et franchise player, vient te parler pendant un temps-mort. Parce que ces mecs, comme beaucoup d'entre nous, on SAIT que tes choix, s'ils sont ceux dont l'équipe a besoin, vont transcender les résultats et changer ta carrière.
Jouer beau c'est Suns basketball, on est d'accord. Mais gagner avant tout doit être ton ADN.

Sans quoi, les rumeurs qui t'envoient quelque part ailleurs dès cet été, parfois même dès la semaine prochaine, elles vont s'intensifier et se concrétiser mon vieux. Ni toi, ni moi n'avons envie de ça. Personne n'a envie de ça.

Let's go Suns !