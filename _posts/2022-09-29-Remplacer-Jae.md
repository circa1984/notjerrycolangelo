---
layout: post
title:  "Remplacer Jae"
author: gregoire
categories: [ Trade, Jae Crowder, Roster ]
tags: [ Roster, Trade ]
image: 'assets/images/10.png'
---

Y'a des séparations plus difficiles que d'autres, surtout quand les sentiments sont forts et que l'attachement ne nous donne pas vraiment envie de rationnaliser. Cet article va parler de ça, comme une longue introduction au suivant qui lui décrira cet été particulier que les Phoenix Suns vivent, où l'on peut autant voir le verre à moitié plein qu'à moitié vide.

Mais ne parlons pas du prochain article. Parlons plutôt ici du départ acté mais pas conclu de Jae Crowder. Le Bossman qui va donc faire ses valises et quitter les violets et oranges après les fabuleuses saisons collectives que nous venons de vivre.

#### Jean Claude Merci

Je déteste qu'on parle à ma place et j'ai donc à coeur de ne pas parler à la place des autres fans de cette franchise. Mais je crois que ce départ blesse la grande majorité des fans des Suns partout dans le monde. Il jouit chez nous d'un capital affectif rare, acquis à l'effort sur le terrain et dans le centre d'entrainement des Suns. Gagné la sueur, au vocal quand il faut encourager, recadrer, leader une jeune équipe en vétérant d'expérience qu'il est.

Jae Crowder c'est LA définition du rôle player au fit parfait dans un 5 majeur de candidat au titre. C'est cette classe de joueurs, rares et prisés, qui connaissent la league, qui connaissent les enjeux, la route, les heures interminables, bref le package complet recquis pour être parmi les 3 ou 4 équipes qui attaquent la saison en se disant : "on peut aller au bout les frérots".

C'est ce que toute la Suns nation s'est dit ces deux dernières saisons. Et peu importe ce qu'on se dit à quelques semaines du démarrage de la prochain, rien que pour ces deux dernières saisons, Jae, merci pour tout.

#### A quoi dit-on aurevoir ?

Si les ruptures comme celle-ci sont difficiles, nous restons néanmoins dans un business âpre, où le moindre détail finit par faire un gap incroyable entre un statut de contender en 2022 et un statut de contender en 2023.
Me faites pas dire ce que j'ai pas écrit : non, ce détail c'est pas Jae du tout. D'ailleurs, y'en a sans doute pas qu'un de détail. Y'en a même sans doute une guirlande entière et James Jones est sans doute en train d'essayer de régler chaque point avec la minutie qu'on lui connait.

Le profil de Jae, c'est une vraie galère à trouver en un seul joueur. S'imaginer qu'on va pouvoir trouver un remplacant fait du même bois, à mon humble avis, c'est se fourrer une écharpe de la taille d'une poutre médiévale en plein oeil. J'ai beau être petit-fils de charpentier, j'imagine pas le projet le plus fun de l'histoire.

Un profil fait de combatitivité, de goût de l'effort, de leadership, d'intransigeance, de camaraderie,  pour un ratio statistique honnête. Qui a ça dans la league à proposer ? PJ Tucker et Thaddeus Young sont les deux noms qui me viennent le plus vite sur la langue. Deux noms intouchables. On passe donc à autre chose.

#### Quelles solutions alors ?

L'apport de Jae ne tient clairement pas aux statistiques. Elles ne sont pas franchement folichonnes pour Bossman. Non, son apport est ailleurs. La solution vers laquelle James Jones semble s'orienter pour palier à son départ est une solution que je trouve rationnelle et construite autour d'une bonne grosse dose de sang froid : aller chercher les skills du Jae dans les joueurs que nous avons déjà, ou que nous venons de signer, en les responsabilisant.

On sait donc déjà que Cam Johnson va devenir un titulaire. Moi je crois dur comme fer que ce gars peut nous faire une saison de MIP, même et surtout si coach Monty construit davantage de systèmes autour d'Ayton. La production statistiques du spot 4 titulaire des Suns va donc être démultipliée, à n'en pas douter !

Ish Wainwright, en complément sur le banc, va pouvoir jouir de davantage de minutes de jeu. Ce ne serait pas immérité : un tel coeur et un tel engagement sur le terrain me rappelle les meilleures années de Ronny Turiaf, dont j'adorais l'énergie dépensée sans compter sur les parquets de la NBA, quand bien même ses skills techniques n'étaient pas ceux d'Hakeem.

Dario Saric, enfin de retour, va pouvoir prendre la suite du liant parfait dans la recette du playbook de Monty Williams. Clairement le joueur parfait pour apporter du QI, de la grinta et de la vision dans le jeu des Suns, en 1st ou 2nd unit d'ailleurs.

Bref, James Jones a déjà recruté ce qui permettra de palier au départ de Jae Crowder. Ce qu'on peut donc espérer de ce trade, à moins que je me trompe (ce qui n'est évidemment jamais impossible), c'est recevoir quelque chose que nous avons pas : un scoreur en sortie de banc, par exemple. mais ça, c'est une autre histoire.

Bon vent Jae. Même si la rupture avec le front office des Suns est difficile, nous, les fans, on te regarde partir en te souhaitant le meilleur, où que tu attérisses.