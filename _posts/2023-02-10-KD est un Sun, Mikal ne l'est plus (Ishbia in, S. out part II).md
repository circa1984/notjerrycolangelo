---
layout: post
title: "KD est un Sun, Mikal ne l'est plus (Ishbia in, S. out part II)"
author: gregoire
categories: [ Suns, KD, Mikal ]
tags: [ Kevin Durant, Mikal Bridges, Cam Johnson, Mat Ishbia, Trades ]
image: 'assets/images/17.png'
---

Si on m'avait dit un jour que le sentiment qui prédominerait en moi à l'arrivée de Kevin Durant dans ma franchise de coeur serait la tristesse, j'aurais sans doute rigolé à gorge déployée. C'est vrai quoi, on parle de Kevin Durant, dans les jerseys historiques des Suns, avec son numéro 35 floqué et son jump shot soyeux, son mental de tueur... Bon sang.

Pourtant c'est bien le cas, je suis extrêmement triste, depuis 36h, alors que KD est un cactus. La raison, vous la connaissez toutes et tous évidemment. La contrepartie de ce trade. Un crève coeur terrible, avec des aurevoirs douloureux parce qu'impossibles. Une violence parce qu'on est attaché à ces gars, à l'équipe qu'ils formaient dans ce vestiaire que tout le monde croit pété alors qu'il est, ou était, l'un des plus cools de toute cette foutue league...

#### Jean Claude Riche

C'est le moment gauchiste de l'article, préparez-vous, échauffez-vous.
Les Suns cette saison, c'est une formidable dynamique au départ, avec un Booker candidat MVP, Ayton qui montre de très belles choses, et un collectif parfaitement rodé autour. Une dague qui fend toute la league au Footprint Center, et qui pointe premiers à l'Ouest devant les désignés favoris. Surprenant ? Non. Les deux saisons précédentes ont construit ce groupe audacieux, désormais concentré et suffisament fort pour imaginer les plus belles choses arriver dans l'Arizona. Contender ? Oui. On a pas dit ce mot parce que le mode silencieux nous allait bien, mais tout le monde savait que prendre les Suns dans son parcours en Playoffs, c'était s'assurer une bonne suée. On savait, la fanbase savait. Malgré les frustrations, malgré les blessures, on savait que si on faisait Top 6 de l'Ouest avec cette équipe, TOUT était possible. Tout.

Et puis Matt Ishbia arrive.
Nouvel owner des Suns, en remplacement d'un harceleur dont on ne citera plus jamais le nom ici. Et avec cette arrivée, des espoirs monumentaux. Si on savait que cette équipe était capable de tout, on n'en niait pas pour autant les besoins d'ajustements à la touche, pour aller chercher ce profil un peu scoreur qui manquait en sortie de banc, par exemple. On en a parlé en long en large ici sur ce blog...
Avec ces espoirs, on en a presque oublié la réalité de ce que signifie et implique un changement dans la top hierarchie d'une franchise NBA : des changements énormes.

En 2008, Steve Kerr, actuel coach des Warriors, arrive aux Suns en tant que GM. 2008 c'est cette période où Nash est déjà deux fois MVP, et second des votes juste derrière Dirk MVP juste après. C'est le summum du Suns Basketball et d'une culture basket accelérée, limpide, sublime à voir devant nos écrans.
Le premier move de Kerr est de rompre avec cette culture. Il la balance comme l'eau du bain, en envoyant Shawn Marion au Heat pour récupérer un Shaquille O'Neal qui entame déjà sa fin de carrière. Quelques mois plus tard, Kerr trade également Boris Diaw et Raja Bell. L'identité de l'équipe, les chouchous des fans, tout ça balancé en l'air pour que finalement, Steve Kerr quitte son poste en 2010, deux saisons plus tard... Sans bague évidemment.

Matt Ishbia a également choisi la violence. Comme tous les types qui se retrouvent en situation de pouvoir. Si le trade de KD était en place l'été dernier et que l'ancien owner l'a fait capoté, Matt Ishbia s'est occupé de le réactiver et de le finaliser en 24h après sa prise de contrôle des Suns. Pleins pouvoirs = vitriol, javel, lacrymo. On a que nos larmes pour se souvenir de la beauté de cette équipe construite à merveille, pour se souvenir de la pureté des shoots de Cam Johnson, et des sourires du défenseur élite qu'est Mikal Bridges. Ces deux là sont désormais des Nets, en pleine saison formidable pour eux et pour les Suns.

C'est vrai. Cam J était titulaire poste 4, en constant progrès depuis le début de sa saison. En pleine prise de confiance et de place dans cet effectif où il avait de quoi exprimer tout son talent. Mikal ? Début de saison 100% collectif, à son habitude. Et puis les 75% de titulaires blessés l'ont vu être jeté en patûre dans un rôle imprévu : sa prise de leadership, de responsabilités, en l'absence de toutes nos options offensives. Dans le dur le temps d'apprendre le job, Mikal sort au moment du trade d'une série de plus de 10 rencontres à plus +20ppg... Un décollage stratosphérique que tout le monde attendait : on avait enfin ce joueur qui allait prendre des shoots en relai de Booker, et attirer les défenses, permettant de libérer la place pour Devin, Cam J et DA... C'était parfait. Vraiment, absoluement, parfait.

Un riche a foutu son ego sur la table et décidé que les chances de cette équipe, réelles, pures, valaient moins que son coup de pub pour montrer que c'est lui le patron. Le trade de KD aurait pu attendre l'été 2023, large, après un éventuel parcours de nos Suns ère Mikal, si attachants et prometteurs. Mais un riche a décidé que les promesses valaient moins que sa petite promo égotique. Alors voilà, on a perdu Mikal, Cam J, et même Dario Saric en seulement quelques heures...

#### Peser les chances

On a vécu l'été dernier en apnée autour de l'idée de voir KD rejoindre les Suns. C'était vertigineux quand on y repense. Et puis voilà, on y est.
Juste avant la saison 92-93, les Suns avaient envoyé Hornacek, gros chouchou de la fanbase des Suns, aux Sixers en échange de Charles Barkley, futur MVP sous le maillot des Suns et futur finaliste NBA... On connait donc ce genre de trade où on envoit une partie de son âme contre ce qu'on imagine être une chance plus grande de voir son équipe taper la bague. Spoiler : défaite en finale contre les Bulls de Jordan, vous connaissez l'histoire...

Chance plus grande, c'est un concept que je débattrais volontiers, pas fondamentalement convaincu que ce soit réel dans le cas de figure que nous visons en ce moment. Je l'ai dit plus haut, on était contender, maintenant on est favori. La seule différence majeure, c'est qu'on est identifiés comme dangereux par les observateurs et les story-tellers. Ok. Est-ce que les Suns avant KD n'avaient pas leur chance ? Est-ce que l'arrivée de KD multiplie réellement les chances de façon radicale ?

Je sais pas. je suis peut-être un poil trop soupe-au-lait en ce moment pour répondre. je serai de bonne foi quand on fera les comptes à la fin de la saison, et bien sûr que si Book et KD ramènent la première bague à Phoenix, je serais le premier heureux. D'autant que ca voudra dire que Mikal, Dario et Cam J auront la leur dans le lot.
Encore une fois : je n'ai rien contre KD et je suis TRES excité à l'idée de le voir s'intégrer chez les Suns et de les voir lui et Booker martyriser toutes les défenses de cette league. Sur le papier, c'est parfait.

Nos chances aujourd'hui sont ce qu'elles sont : vraies, tangibles, craintes. Et c'est très bien.

#### Kevin Durant est un cactus

Je ne sais même pas quoi écrire. Nous, la fanbase des Suns, on a pas connu ce genre de gros move depuis 1993 ezt le trade de Barkley. Honnêtement. J'ai parlé du trade de Shaq un peu plus haut, mais c'était loin d'être prime Shaq quoi... Non, ca fait réellement 30 berges qu'un trade de si haute altitude n'a pas agité notre franchise avec autant d'ampleur.

L'arrivée de Kevin Durant rivalise désormais assez bien avec celle de Chuck. KD est l'un des 3-4 meilleurs joueurs de cette league. Une super superstar, un profil unique, légendaire, iconique. Et il est désormais un cactus, avec quelques années de contrat.

Il va apporter sa constance, sa patience, son mental de tueur, son énorme pouvoir offensif, son excellence défensive (qu'on oublie trop vite), mais surtout : son très gros QI stratégique. A ce titre, nulle inquiétude quand à son fit dans un effectif très intelligent, sous un coach qui valorise beaucoup le QI basket. Qui de Durant ou de Book sera le Franchise Player ? Aucun des deux ne va se poser la question, ils s'en moquent, alors moi aussi. Qui de Durant ou Booker va prendre les shoots de la gagne dans le money time ? Les deux savent que ce sera décidé au moment, en grande intelligence et sans ego. Et ça, c'est de l'or pur dans une équipe.

Je ne ferai pas de paragraphe ou d'article spécial Mikal Bridges. Si vous lisez ces lignes ou suivez mon @ sur twitter, vous savez. Vous savez que c'est mon all time favorite et quelle _douleur_ je vis de le voir ne plus être un Sun.

Mais je me convainc que le sacrifice est rationnel parce que Kevin Durant est le seul joueur NBA en activité qui valait tel sacrifice. Si quelqu'un dans cette league valait une telle amputation d'âme et de vie dans un groupe en compensation de son arrivée, c'est Kevin Durant. Alors je me fais lentement à ce trade, un peu comme je peux, en regrettant son timing horrible et égotique, mais en me disant que c'était le seul trade d'une telle ampleur qui pouvait avoir lieu. Et on l'a eu.

Kevin Durant est un cactus.
Let's go Suns !