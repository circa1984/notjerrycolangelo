---
layout: post
title: "Booker, Monty, Ayton et les autres"
author: gregoire
categories: [ Saison, Suns ]
tags: [ Bilan, Saison, Monty, Booker, Ayton ]
image: 'assets/images/14.png'
---

Disons le sans tarder, en intro de cet article : si le mois de Décembre confirme le mois de Novembre 2022, le premier tiers de la saison des Phoenix Suns sera exceptionnel.
Parce que oui, ce mois de Novembre est en tout point une réussite. Oui, les Suns ont toujours le meilleur bilan à l'Ouest, talonnés par les excellents Pelicans et Nuggets. Oui, ça joue bien, oui, le groupe vit bien.

Non tout n'est pas parfait. Mais tout n'a pas à l'être. C'est la NBA, la perfection n'existe pas, mais ce mois de Novembre nous amène quand même pas loin de ce que cette équipe peut donner quand elle joue parfaitement.

Retour sur les faits.

#### Jean Claude Récompenses

Cam Johnson est toujours out, et pour un petit moment encore. Et ça doit bien faire 3 semaines que Chris Paul doit revenir bientôt.

Sur les deux dernières semaines du mois, on a aussi eu quelques lumières à propos d'un trade incluant Jae Crowder, toujours aux abonnés absents, et qui nous grille un précieux spot dans notre rotation. Mais ces lumières se sont vite éteintes et l'équipe a continué son train comme si de rien n'était.

De quel train on parle ? 10 wins, 5 défaites. On va revenir là dessus juste après...

Mais de quelle façon ? Brillante, y'a aucun autre mot. Pourtant, sur la première moitié du mois, on alterne le bon et le moins bon, avec tantôt une défaite (souvent contre des équipes à notre portée en plus...), tantôt une victoire. De petites montagnes russes avant la grosse frappe dans la lucarne de notre trio. Par trio j'entends Monty Williams, Deandre Ayton et Devin Booker. Et commencons par le coach.

Monty Williams a passé son mois à donner confiance en son banc. On a vu des mecs prendre de précieuses minutes et en faire quelque chose de très très bon. Même quand le bench est sur le terrain, on voit une application et une volonté de jouer des systèmes très bien dessinés par le coaching staff. On sent même quand nos starters ne sont pas sur le terrain, que ce Suns basketball dont Monty Williams est l'architecte, infuse dans toutes l'équipe, avec de très bons résultats. L'exemple qui saute le plus aux yeux c'est évidemment Damion Lee, qui nous fait une saison remarquable, particulièrement du parking où il pointe à la troisième place des shooters les plus efficaces de la league cette saison. Et oui, c'est 100% grâce à un Monty Williams qui sait tirer le maximum de ces gars là. [Coach of the month du coup le Monty](https://twitter.com/Suns/status/1598429659779063808). Mérité, carrément mérité.

Ensuite il va falloir parler de Deandre Ayton. Vous savez, ce joueur qui ne voulait pas petre là, qui était trop soft et pas motivé, qui pensait davantage à jouer à _Call of_ qu'à écraser des peintures, qui devait être trade en cours de saison, et blablablabla ?
[Joueur de la dernière semaine du mois](https://twitter.com/NBA/status/1597327996096237569), et énorme séquence pour lui à partir du dernier tiers du mois. A partir de la grosse poussette de cette ordure de Pat Beverley en fait. Depuis 5-6 games, DA plane sur les raquettes et y construit des immeubles. du 25/10 comme hier soir face aux Spurs, c'est devenu normal. Visuellement on le sent dedans en balle, disponible, agressif, sans renier ses fondamentaux techniques. J'avais cru cet été à ce réveil, j'avais cru à une ligne de stat de 22/12 sur la saison. Je suis content d'avoir pris ce pari là et de voir Deandre donner tort à ses detracteurs.

![Deandre Ayton en mode Dominayton](https://rallythevalley.fr/assets/images/ayton-nv2022.png)

Et il faut finir sur le boss, le crack, le cheat code des Suns. Devin Armani Booker. [Player of the Month à l'Ouest](https://twitter.com/Suns/status/1598421988510699520) (Jason Tatum prend la récompense à l'Est). Des stats inc-royales. Mais tiens, ne parlons pas des stats. Parlons juste de l'impression visuelle et de ce qu'on voit sur le terrain : une maitrise complète du jeu dans chacune des possessions, une motivation en cuir tané, qui ne s'arrête jamais, une concentration épaisse et intense, et une confiance à toute épreuve. Il est bouillant Devin. Il est bouillant pour lui, en signant des matches incroyables comme un 51pts, 6 passes contre Chiacgo, à 80% au tir... en seulement trois QT... Non vraiment Book était intouchable ce mois-ci. Il a marché sur la league et son talent, son mindset et son jeu ont complètement influencé le reste de l'équipe. Volontiers meneur de jeu, à la baguette pour placer, orienter, libérer ses coéquipiers, il est la source de vie du jeu des Suns, et sur ce mois de Novembre, elle semble intarrissable. Tant mieux. Calibre MVP ? Laissons parler les bavards.

![Devin Booker en mode cheat code](https://rallythevalley.fr/assets/images/booker-nov2022.png)

#### Le Bilan et la suite.

10 Wins donc pour 5 défaites.
Mais surtout quelque chose qui saute aux yeux. On parait imprenables à domicile, ce qui est une excellente chose. En revanche, on parait aussi un peu plus en difficulté en déplacements. Rien de super grave mais nos défaites sont principalement en déplacement.

Il y a 9 réceptions à domicile, et on en laisse une seule, aux Blazers, début Novembre. 8/9 victories donc quand on joue sur nos terres. Mais seulement 2 victoires sur nos 6 matchs en déplacement, y compris contre des équipes que nous DEVONS battre (le Magic par exemple). C'est un point sur lequel il va falloir être vigilant en Décembre et sur le reste de la saison. gagner à l'extérieur et savoir aller chercher des résultats chez les autres, c'est très important lorsqu'on a les objectifs que les Suns peuvent avoir cette saison. En Play-Offs, gagner chez soi ne suffit plus.

Néanmoins, rien d'alarmant. La suite arrive très vite et le mois de Décembre commence sur les chapeaux de roue. Hier, blowout contre les Spurs, mais dès cette semaine, que des confrontations face à de très grosses équipes : les Mavs, puis Boston, et deux fois les Pelicans pour finir une copieuse semaine. De l'adversité et des très gros challenges collectifs et individuels. Une semaine de tests pour qu'on jauge où ces Suns en sont face à des équipes qu'on retrouvera sans aucun doute après la régulière.

On a hâte. On a très très hâte de voir si Deandre Ayton va continuer sur sa lancée, si Booker va confirmer son cheat code, et comment Monty va poursuivre son travail de sape autour des systèmes, du banc et des rotations. Confirmer, c'est le maitre mot qui semble là pour rythmer toute la saison. Chaque victoire doit être confirmée le soir suivant, parce que personne dans cette league depuis deux ans ne sait mieux que les Suns que rien n'est acquis en NBA.