---
layout: post
title:  "Sarver out : phase 1"
author: gregoire
categories: [ Franchise, Sarver ]
tags: [ Front Office, Franchise ]
image: 'assets/images/9.png'
---

Un article spécial, pour une humeur particulièrement heureuse, je dois l'avouer. D'habitude, des changements aussi importants dans une franchise NBA sont autant de signe d'instabilité ou de difficultés en cours ou à venir. Dans le cas des Suns en cette rentrée 2022, confessons qu'on est plutôt sur un soulagement, tant cette affaire Sarver était lourde sur la franchise, et tant l'issue qui s'annonce parait salvatrice.

#### Jean Claude Privilège

Clairement, cet été, l'affaire Sarver était le coeur de mes attentes concernant la franchise de l'Arizona. Ouais, ces histoires de culture matchiste et raciste, je peux pas les encadrer. Nulle part. Alors les tolérer dans ma franchise de coeur aurait été faux-cul. Tolérer que le président de la franchise des Suns, Robert Sarver, ait des comportement sexistes, des mots racistes, et impreigne une culture horrible de management dans toute la structure des Suns et du pendant WNBA que sont les (fabuleuses) Mercury, non. Pas du tout.

Y'a pas de place pour ces abus. Donc ca dégage. Ca aura pris son temps et ca aura fait parler. Trop de temps, et trop de paroles maladroites d'ailleurs. On en parle du communiqué officiel de la NBA qui liste les sanctions ? Ridicule. Doux. Soft comme Ayton dans les mauvais soirs, quand on a besoin qu'il soit agressif et intenable.

Haureusement, et comme d'habitude, parce qu'on se repose trop sur eux, il y a des bouches qui se sont ouvertes, des bouches de leaders qui n'ont pas le temps et la souplesse d'Adam Silver en la matière : Lebron James en tête, Chris Paul, et puis en domino plein d'autres. Cétait exactement ce que j'espérais. Que cette histoire devienne un conscensus moral, éthique, sur lequel on continuera de construire une league moins conne.

#### Quelle genre de vente ?

Maintenant, le futur est à la passassion. En forme de vente, sans doute, selon les dernières infos que nous avons coté Sarver.

Les plus inquiets dans la fanbase s'imaginaient déjà la franchise être avalée par les projets d'expansion à Seatlle et Las Vegas. Dans ces plans farfelus, Phoenix perdrait sa franchise NBA, qui aurait déménagé dans l'une des deux autres villes, et aurait subit un rebranding en mode Supersonics ou Las Vegas Aces ou que-sais-je.

En vrai, cette théorie ne tient pas, simplement parce que notre franchise ensoleillée est compétitive, valuable comme on dit de l'autre coté de l'océan atlantique. Et c'est très cool d'ailleurs. parce que c'est signe qu'elle sera attractive et intéressante pour des investisseurs.

Dans les prochains semaines, les prochains mois, l'affaire se soldera, éspérons, pour un avenir toujours plus radieux dans le desert arizonien. Et éspérons qu'avec les prochains personnes aux commandes de la franchise, on aurait pas à rougir d'affaire comme celle de Sarver. Parce que Fuck Sarver.