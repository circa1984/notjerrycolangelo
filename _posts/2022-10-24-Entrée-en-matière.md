---
layout: post
title:  "Entrée en matière"
author: gregoire
categories: [ Saison, Suns ]
tags: [ Booker, monty, Landale ]
image: 'assets/images/12.png'
---

Voilà. La saison NBA a redémarré. 3 matches derrière nous, contre les Mavs d'abord, puis chez les Blazers et les Clippers. 3 matches qui signent la première semaine des Suns dans la saison 2022-2023 de NBA. Et on a vu des choses sur ces trois matches. Des choses qui infirment ou confirment ce qu'on avait imaginé pour cette saison et cette équipé à peine remaniée cet été.

Faisons le point. Ayton est-il rayonnant comme on le voudrait ? le banc est-il éclaté au sol ? Paul est-il complètement rincé et déprimé après s'être fait graillé par la défense des Mavs l'an dernier ? Booker est-il moins bon que Jaylen Brown ?

Ptdr.

#### Jean Claude Revanche mais on s'en fout

Les Suns sont allé cherché une victoire en yoyo face aux Mavs. Un match élastique où on a tantôt dominé de 20pts, tantôt vu les Mavericks revenir comme dans du beurre... Booker ? grosse impression. Ayton ? Solide et impliqué. Mikal ? Toujours le chouchou. Le second unit ? Une catastrophe. Chaque foisque celle-ci est sur le terrain, les Mavs revenait. Incapables de trouver leurs marques, ils étaient clairement notre point faible sur ce match. On reste quand même victorieux sur notre parquet, 107-105.

Un départ assuré par la victoire, mais pas complètement rassurant. J'ai beaucoup aimé la capacité de l'ensemble de l'équipe à rester dans le match, focus, dédiée, appliquée. Déterminée à croire jusqu'au bout qu'ils pouvaient et allaient gagner ce match. C'est m'élément le plus important à mon sens cette saison : l'esprit de l'équipe et sa détermination à gagner. On arrête les conneries. On arrête les bouffoneries et on va chercher les victories chaque soir avec le même _drive_. J'ai vu de ça dans ce premier match, notamment chez Booker, et j'ai aimé.

Puis déplacement chez les Blazers. On y perds au clutch, 113-111. C'est regrettable mais on a laissé trop de place aux Blazers et Lillard n'a pas besoin de grand chose pour claquer des perfs énormes et clutchs. On a mangé sa démo toute la soirée et deux lancers d'Ayton ratés plus tard (sans lui jeter la pierre), Portland gagne à domicile, et on se tourne déjà vers les Clippers de Kawhi et Paul George.

Comme je l'ai dit chez [Valley Oop](https://twitter.com/Valley_Oop), le podcast dédié à l'actu des Suns, [dont j'étais l'invité du dernier épisode en date](https://twitter.com/gregoirembv/status/1579511561840099330), ce début de saison peut déjà être déterminant.  Il y a effectivement une séquence de 8 matches où on affronte uniquement des équipes de l'Ouest, et notamment des équipes qui imaginent et prétendent aux mêmes places de classement de conférence que nous. Ces matches sont donc importants pour le seeding en fin de saison, où, vue l'adversité et le niveau général de l'Ouest, il risque d'y avoir une bataille incroyable. Faut donc qu'on soit invisifs sur cette séquence, et rapporter un max de victoires. Et si on a raté le coche chez les Blazers, on peut dire qu'on a bien retourné l'ambiance chez les Clippers.

Victoire 95-112. Comment ? En défendant le plomb. Une défense frérot, de l'horlogerie suisse. De quoi jeter l'adresse des Clippers à la poubelle dès le premier QT. D'autant plus déterminant qu'à coté de ça, Devin putain de Booker termine de démontrer que cette saison, y'aura peut-être pas de débat. On y reviendra... A coté de lui, Ayton est plus discret, Paul ne trouve pas la mire mais distribue bien, Mikal défend le plomb donc on lui passe ses lanquements offensifs, et la second unit s'organise de mieux en mieux. Une victoire qui fait du bien.

#### Premiers signes d'un renouveau

Je suis très heureux. Ce début de saison des Suns apparait comme une confirmation de quelques souhaits que j'avais émis ici, sur twitter ou chez Valley Oop. Lesquels ? Ceux là :
- **Ayton est plus impliqué** : c'est pas une nouvelle, j'ai sauté dans le train Ayton y'a un moment et je pense que mobilisé autour de systèmes faits pour lui, ce très jeune pivot sera léthal. Ce début de saison nous prouve que Monty a pris la (bonne) décision de créer des systèmes autour de lui, et de l'inclure davantage dans les plans de jeu de l'équipe. Qu'il ne soit pas toujours en réussite ou qu'il reste du pain sur la planche pour qu'il soit léthal, c'est possible : ca prend un peu de temps d'affuter une lame restée trop longtemps dans le placard.
- **Le jeu est moins dépendant de Chris Paul** : En ce début de saison, le temps de jeu de Chris Paul est réparti entre les starters et la second unit. En effet, quand les remplacants sont sur le terrain, on voit par grosses séquences Paul à la mène. Puis Cam Payne être présent lorsque les trois axes principaux de l'équipe (Book, Mikal et DA) reviennent. Intéressant donc de voir Paul organiser le jeu de la 2nd unit. Intéressant aussi de le voir moins détenir toutes les clefs lorsque le 5 majeur est sur le terrain. Monty l'a bien compris : on a besoin de mieux r&partir les responsabilités, et d'être moins dépendants de CP3. Les stats du meneur de bientôt 38ans ont donc maigri au scoring. Tant mieux, d'autant plus que...
- **Booker assume son statut de leader** : ... Devin Booker a de fait pris le relai et les responsabilités qui incombaient à CP3 l'an dernier. C'est lui qui porte, distribue et leade le jeu des Suns cette saison. C'est lui qui met la team sur ses épaules, et montre la voie. Et de quelle façon ?

![le début de saison de Booker](https://rallythevalley.fr/assets/images/Bookerstart2023.png)

Vous avez bien lu.
**32 pts - 5 passes, 53% au shoot, 53% du parking, 66% de true shooting**.
La pro-pre-té. Je sais pas si tout le monde se rend compte, mais réaliser cela tout en impliquant les autres et en leadant l'équipe, à cet âge là, c'est de très bon augure pour le futur de la franchise. Enfin, Devin Booker se transforme en ce qu'il devait devenir. Il manquait juste ce petit truc, cette responsabilisation, qui aujourd'hui lui permet d'embrasser enfin cette partie de son potentiel qui va faire de lui le joueur dominant qu'il est sur ce début de saison. Tant mieux. Ca fait trop plaisir. Pour lui comme pour toute la franchise et la fanbase.

#### Autre chose chef ?

Le banc.
Faut qu'on discute de ce banc. Je pourrais parler de Cam Johnson, un peu discret sur ce début de saison, mais le kid a sans aucun doute besoin de s'adapter à son nouveau rôle, et grosse flemme de l'enfoncer pour 3 matches pas pire mais pas oufs.
Non, parlons du banc.

Jae Crowder n'est pas encore transféré alors que cet article se rédige. Il nous manque donc tout de même un renfort de choix, de poids. On aura pas une star hein, mais Jae devrait pouvoir partir en nous permettant de choper une contribution au jeu relativement importante pour l'équipe, et particulièrement pour le banc.

A ce jour, le constat est mitigé. C'est pas pire hein, me faites pas dire ce que j'ai pas dit. Mais c'est pas la grosse éclate quoi. Même si au fond, je préfère, comme souvent, me concentrer sur les aspects positifs, il faut tout de même rester vigilant au jeu de ce banc, qui souvent, fragilise ce que les titulaires ont accompli juste avant. Par manque de consistance d'abord. Shamet a raté les deux premiers match et trouve une adresse de 40% (honnête, très honnête même) pour son retour contre les Clippers. Ish Wainwright rate aussi quelques matches, etc etc. Au délà des absences, est-ce que je parle de Cam Payne ? Peu en verve, une adressd fadasse, mais tout de même, son langage corporel est plus positif que l'an dernier. laissons lui encore un peu de temps...

Il y a néanmoins des choses un peu lumineuses, qui perso me réjouissent. Je veux parler de l'énergie de T.Craig (face aux Clippers par exemple). L'an dernier, on sentait qu'il avait presque eu du mal à intégrer la dynamique du groupe. mais cette année, il semble retrouver ce qui fait de lui un joueur intéressant : gros coeur, grosse énergie, dimension physique capable de géner des ailiers comme des pivots. On a besoin d'un Craig mordant, même si ses lignes stats ne sont pas celles de Karl Malone (et d'ailleurs, c'est absolument pas ce qu'on lui demande).

Mais surtout : JOCK LANDAAAAAALE LES FREROOOOOOTS !!! Mais quelle impression fantastique laissé par notre jeune intérieur ! Déterminé, appliqué, chaud comme un brasero dans un barbecue de piments, ses stats ne rendent pas hommage à ce qu'il apporte à l'équipe en ce début de saison. Les interrogations sur la profondeur de note effectif à l'intérieur étaient nombreuses avant le début de saison. D'autant plus quand on a compris très vite que Saric allait pas être intégré à l'équipe tout de suite. Mais Landale régale. Il donne tout ce qu'il a sur les parquets et avoir un joueur qui s'implque à ce point, c'est par-fait.

#### Conclusion ?

On est encore tôt dans la saison, avec seulement 3 petits matches joués. S'ils devraient logiquement rassurer un peu une fanbase parfois amère sur l'été des Suns, ces matches importants ne sont tout de même pas assez nombreux pour en tirer de grandes et larges conclusions.
Les Suns vont aborder la seconde semaine avec un net rating de 6 pts (Off Rating : 112.37, 14ème / Def Rating : 106.32, 7ème). C'est loin d'être honteux, mais on sent que cette équipe a sous le capot de quoi faire mieux, notamment en attaque, à coté de Book.

Au délà du pur terrain, quelques points sont encore à régler côté Suns. Le trade de Jae Crowder, l'inclusion ou le départ de Saric (que je ne souhaite pas mais qui est possible), savoir qui va intégrer l'équipe en échange, et évidemment la vente et le départ de Sarver, qui est clairement ce que j'attends avec le plus d'impatience.
Bref, déjà hâte de faire un point après la seconde semaine !