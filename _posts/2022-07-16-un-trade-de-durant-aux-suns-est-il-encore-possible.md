---
layout: post
title:  "Durant aux Suns : un trade est-il encore possible ?"
author: gregoire
categories: [ Trades ]
tags: [ Durant, Trades, Nets ]
image: 'assets/images/4.png'
---

La saga de l'été n'est pas sur TF1. Elle est sur internet, et se déroule entre Brooklyn et différentes villes américaines, et notamment celle de Phoenix. Au début de l'histoire, il y a un Kevin Durant qui veut rejoindre les Suns (ou le Heat de Miami). Une nouvelle qui a fait l'effet d'une bombe et sur laquelle il n'est pas utile de revenir tant l'eau a coulé sous les ponts depuis, comme les larmes sur les joues de celles et ceux qui avaient mis 100 balles et un mars sur un titre des Nets l'an prochain.

Avec le nouveau contrat d'Ayton et l'ensemble des clauses qui le rendent difficilement transférable d'ci un an, les Suns, un temps destination favorite des insiders qui suivent l'affaire, ont chuté soudainement dans la liste des endroits que KD pourrait rejoindre pour la prochaine saison.
Avant cela, les discussions ont sans doute été intenses entre James Jones et Sean Marks. Mais elles n'ont surtout jamais abouti.

Y'a t'il donc encore un espoir pour la franchise de l'Arizona de voir débouler Kevin Durant ?

#### Jean Claude L'espoir-fait-vivre

Flex, un insider spécialiste des Suns, croit encore à la venue de KD aux Suns. La preuve avec ce tweet tout plein d'espoir, lorsque les Suns ont matché l'offre des Pacers pour Ayton :
> Last thing for the night. The Suns had 48 hours to match the offer sheet but did it in 90 seconds! I repeat 90 seconds! I assure you if they thought their chances of losing out on a potential KD deal was at stake they would have taken the full 48 hours to really think things out!
[FLEX From Jersey - Twitter, Juillet 2022](https://twitter.com/FlexFromJersey/status/1547792246228955136)

Les sites de trade-machines n'ont jamais autant chauffé. Les serveurs sont harcelés par la communauté Suns qui souhaitent absolument trouver LA solution à ce trade. Alors, oui, techniquement il en existe plein. Il en existe même qui n'incluent pas Mikal Bridges et Cam Johnson. Sont-elles réalisables pour autant ?

Flex n'est pas le seul, à tout imaginer pour que ce trade se fasse. Et finalement, on peut y croire, ca n'engage à rien.
On peut imaginer ce monde où James Jones arrive à convaincre Sean Marks du bien-fondé de sa proposition et de combien elle est gagnante pour les Nets. On peut imaginer ce monde sans prendre de LSD, on peut le faire.

Du coté de la communauté des Nets, on peut dire que le mot d'ordre c'est HELL NO!
Et on peut imaginer que c'est également le mind set de Sean Marks...

#### Sean Marks vs James Jones

S'il y a quelque chose qui peut rassurer la fanbase des Suns, c'est de se dire que James Jones est aux manettes. Ce type a l'intelligence et la capacité à faire sortir positivement la franchise de cet été qui ne s'annoncait pas des meilleurs il y a encore quelques petits jours. Après tout, ce qu'il construit depuis qu'il est là prouve sa valeur, ses compétences, sa force.

En face de lui, à Brooklyn, on a sans doute appris ses leçons du trade d'Harden aux Sixers. Un trade difficile, étiré dans le temps, âpre dans les négociations, et doux-amer dans le résultat. Un trade formateur néanmoins, pour Sean Marks et son équipe. Nul autre qu'eux auront appris de la féroce opiniatreté de Daryl Morey dans cette histoire.

Il parait qu'aujourd'hui, le président des Suns Robert Sarver est dans son avion vers la côte Est... La fanbase des Suns aime à croire qu'il y va pour rencontrer les dirigeants des Nets et essayer tout ce qui est en son pouvoir pour ramener KD à Phoenix. On verra bien. Ne nous affolons pas.

Ne nous affolons pas parce que dans tous les cas, notre saison prochaine a déjà son lot de problématiques à régler, et d'espoirs à transformer en réussites. Deandre Ayton est de retour et il appartient au staff et aux cadres de l'équipe de lui donner la place que son contrat lui promet : une place prépondérante, importante, dans les plans de jeu des Suns pour la saison qui arrive. C'est déjà beaucoup à réaliser, et si KD venait à s'ajouter à cette équation, il n'en changerait pas la nécessité de la voir se résoudre positivement.
