---
layout: post
title:  "L'allégorie du verre"
author: gregoire
categories: [ Saison, Suns ]
tags: [ Suns, Saison ]
image: 'assets/images/11.png'
---

Quel été on a vécu ! Un ascenseur émotionnel continu, semaines après semaines, parfois violent, parfois décevant. Et finalement on est là. Dimanche 2 Octobre 2022. La pré-saison des Suns commence aujourd'hui au Footprint Center de Phoenix, contre les Adelaide 36ers de NBL.

Je suis impatient. Le basket et la NBA m'ont manqué, et j'ai hâte de voir ce que cette équipe, remaniée et encore dans une dynamique d'évolution avec le départ de Jae, va aborder la saison.
Les attentes peuvent être grandes : peu de changement vraiment majeurs sur les joueurs cadres et exceptionnels que les Suns ont dans leur roster. Booker, Bridges, Paul, Ayton et Johnson sont bien là. Mais est-ce que pour autant nous qui suivons cette équipe, nous abordons cette saison sereins et tranquilles ?

#### Jean Claude Chaud-Froid

On a passé un été chelou. Ca part de cette élimination contre les Mavs dans ce game 7 horrible de chez horrible où on ne reconnait tellement pas notre jeu qu'on a du mal à réaliser la claque qu'on vient de prendre. Puis, silence jusqu'à ces rumeurs fracassantes autour des Nets et de Kevin Durant : le Slim Reaper souhaiterait se barrer de Brooklyn et une de ses destinations prefs serait Phoenix. Quoi ? Wtf ? Toute la fanbase s'agite, de Twitter à Reddit en passant par de simples blogs comme ici. On est _on fire_ à l'idée de voir KD nous rejoindre.

Pendant ce temps là, Ayton. Furieux et déçu du coaching et de son rôle pendant ce game 7 guillotine, notre pivot starter, premier choix de draft controversé, est Restricted Free Agent : il pourra donc recevoir des offres d'équipes NBA, offres que les Suns pourront matcher. Le suspens est intenable. Les suiveurs des Suns s'imaginent déjà le grand gaillard partir contre rien, ou bien jouent à la trade-machine pour imaginer comment un sign & trade pourrait nous sauver la mise. On se voit attaquer la saison avec Myles Turner au poste 5. Il n'en sera rien. Le Front Office des Suns mettra littéralement quelques minutes pour matcher l'offre des Pacers et conserver Ayton au tarif que son potentiel et sa production mérite.

Enfin, on a les trades ratés. Durant ne viendra pas, et à partir de cette annnonce, le reste s'enchaine. On ésperait Dejounte, il est aux Hawks. On éspérait Shaï, il n'est pas si mal au Thunder. On éspérait aussi beaucoup Bogdanovic, il est en fait aux Pistons. Clarckson ? 0 nouvelle pour l'instant. On est suspendus au trade de Jae Crowder, que les Suns ne conserveront pas en titulaire au poste 4 et qui a donc préféré annoncer que ses valises étaient prêtes.

Alors on regarde notre effectif à l'entame du media-day, à quelques semaines de la reprise et on se demande quelle est la plus value de notre équipe 2023 sur celle de 2022 ? Et là, on a deux moods qui se cotoient dans la fanbase et chez les commentateurs des Phoenix Suns.

#### Le verre à moitié vide

Le symbole absolu de ce double mood, c'est le media-day. Un media-day que les journalistes qui y ont participé ont qualifié de terne, morose, triste. Un media-day dans lequel Deandre Ayton a semblé si déprimé et sur le départ que ca en a affolé des commentateurs.

"_Je n'ai pas parlé à Monty Williams de tout l'été_"

On apprend même que Ayton et coach Monty n'ont pas échangé de tout l'été alors qu'on imaginait toutes et tous que ces deux là avaient beaucoup de choses à se dire et à assainir. Eh bien non.

C'est clair qu'on peut très vite s'imaginer le pire : la cohésion de l'éqipe est détruite, la bonne humeur du collectif est foutue, l'entente est au plus bas, et la franchise est en train d'exploser. L'affaire Sarver ? La goutte d'eau qui fait déborder le vase. On y est, les Suns vont faire une saison horrible, et on va repartir dès 2024 dans les limbes de la NBA...

#### Le verre à moitié plein

A titre personnel, ce n'est pas du tout en ces termes que j'aborde la saison des Suns. Je n'ai absolument rien à redire à la vision déprimée décrite au dessus, et je comprends et compatis avec les fans des Suns qui sont dans ce mood. Mais clairement ce n'est pas le mien.

J'ai l'impression qu'il faut raison garder. L'impression que plus que jamais il faut faire preuve de sang-froid quand on regarde les images du media-day, et essayer de comprendre ce qui peut se lire entre les lignes d'un été certes tumultueux, mais loin d'être catastrophique.

C'est vrai quoi, quelle est la catastrophe cet été ? Elle est où ? Ayton signe un super contrat qui doit le mettre en confiance. Paul, Booker et Mikal ne sont pas blessés et semblent être en pleine forme. Cam Johson va être responsabilisé en passant titulaire. Nous avons 5 joueurs exceptionnels dans notre 5 majeurs, tous en pleine santé, sous contrat, et sous la houlette du Coach of the Year sortant. Alors pourquoi bouder ? Parce que Durant est forcé de respecter son contrat de 4ans aux Nets ?

Revenons à Ayton au media-day. Je vais prendre sa défense, une fois n'est pas coutume. Notre pivot se fait littéralement enfoncer par la presse et les médias depuis plusieurs longs mois : pas assez agressif, pas assez concentré, trop de jeux vidéo, pas assez d'impact, nanani, nanana. mais le gamin a 23ans, il délivre du 17-11 la saison dans des systèmes qui ne tournent pas du tout autour de lui et qui ne prend quasiment jamais appui sur ses qualités. je pense qu'on peut se mettre un peu dans sa tête et imaginer un gamin complètement saoulé d'être devant des journalistes qui à coup sûr vont encore trouver de quoi se plaindre à propos de lui... Qui ne serait pas dans le même mood ?

Quid de Saric ? Enfin de retour après une absence beaucoup trop longue quand on sait combien ce joueur peut être si précieux dans un collectif et dans les systèmes de Monty. Eh ben ca y est. Il est là, il est de retour, il a joué un Euro Basket avec la Croatie et a donc pu prendre la température du haut niveau pour mieux attaquer la rentrée NBA dans quelques jours.

Et puis pour finir, le mood général du media-day, supposé être triste et terne. Pour rappel, la saison passée, l'ensemble de la NBA nous détestait parce qu'on passait notre temps à être des clowns, à un point qui pouvait devenir agaçant et nous faire passer pour une équipe de prétentieux. je pense donc que c'est plutôt opportun et sage de commencer cette saison sobrement, en partageant plutôt une sensation de concentration, de sérieux, et d'application.
L'affaire Sarver ? La cerise sur le gâteau, une news qui donne le smile !

#### Conclu

La conclusion de cet article est simple. Il y a deux façons d'aborder la saison des Suns. Voir le verre à moitié vide et angoisser autour du manque d'entrain des joueurs, autour du faible de notre banc qui est faible (réalité indéniable), se dire que Ayton va partir après Noel et qu'on arrivera jamais à remplacer Jae.

Et puis il y a l'autre vision, celle du verre à moitié plein : on est en train de se séparer d'un sale connard de président qui nuisait à toute la franchise, on conserve un effectif incroyable, un coach de talent, on semble sérieux et concentrés, et Ayton semble vouloir prouver les choses sur le terrain à l'entame d'un contrat sécurisant pour tout le monde.

Team verre à moitié plein ici. Faut respecter la chance qu'on a d'avoir les joueurs et le coach qu'on. Respecter cette qulité incroyable qu'on a pas eu pendant si longtemps que la bouder aujourd'hui est incompréhensible. Respecter les équipes qui attaquent cette saison en sachant qu'elles ont pas notre chance. Et Respecter celles qui en ont peut-être davantage, et qu'on va devoir taper parce que rien n'est impossible à qui a Devin Booker dans son équipe.

Bonne saison à toutes et tous !