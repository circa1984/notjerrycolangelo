---
layout: post
title:  "Quel playbook pour Monty Williams la saison prochaine ?"
author: gregoire
categories: [ Team, Playbook, Tactique ]
tags: [ Monty, Booker, Paul, Ayton, Mikal, Playbook ]
image: 'assets/images/6.png'
---

La passion pour ce jeu, c'est aussi la passion pour ce qui le rend beau. Et coach Monty Williams est de ceux qui développent un jeu précis, efficace et clairement pas moche depuis le banc. Son playbook les deux dernières saisons, c'est de la fluidité offensive, de l'intelligence, avec la rigueur horlogère dont les Suns ont fait preuve pendant toute la saison régulière. Bien relayés et mis en place par Chris Paul sur le terrain, les systèmes de Monty ont fait mal à toutes les défenses de la league.
Alors que l'effectif est pour le moment quasi-intégralement le même pour la saison à venir, on peut gager que le playbook de Monty devrait être renouvelé autour des mêmes principes. Sans doute quelques ajustements et mises à jour pour continuer d'augmenter l'efficacité de l'équipe, mais rien de foncièrement révolutionnaire pour les cactus. Mais tout de même, une question se pose pour moi : quelles seront les responsabilités de Chris Paul dans le jeu des Suns la saison prochaine ? Voici donc 3 éléments tactiques qui pourraient être clefs la saison prochaine, et qui concernent notamment notre meneur.

#### Jean Claude España

Je vais pas y aller avec le dos de la cuillère : je pense que Deandre Ayton peut être MIP l'an prochain et tourner en 23-12, être infernal match après match, et fermer les grosses gorges des quelques commentateurs perplexes de le voir signer son contrat max cet été. La clef de cette évolution ? Des systèmes complètement dédiés à sa dangerosité offensive. Parce que dans sa besace, Deandre est capable à mi-distance, a plutôt un bon footwork, et finit très correctement de près avec un gros jeu au poste-bas et une excellente finition en mouvement.
S'il est servi au bon endroit et polit un handle qui a besoin de progresser, attention. On va faire confiance au coaching staff arizonien qui va passer son été à remplir des excels de devoirs à la maison pour les mimines de Deandre, et on va juste espérer que le Spain pick & roll de Monty Williams ait dans ses premières options de conclusion un shoot en tête de raquette d'Ayton, ou un roll sale jusqu'à l'arceau pour aller chercher du and one.

Alors quel type de systèmes pour Ayton ?
Un grand classique chez Monty Williams, c'est le [Spain pick & roll](https://www.youtube.com/watch?v=1WlqCf9tRYE). En lui-même le pick & roll est historique à Phoenix, tant il a été poncé et reponcé par des générations de duos iconiques, et particulièrement Steve Nash et Amare Stoudemire. L'update dite Spain n'est pas de Monty Williams lui-même. Je vous invite à lire cet article si ce système vous intéresse particulièrement, [Breaking Down The Phoenix Suns Spain Pick & Roll Plays](https://halfcourthoops.substack.com/p/breaking-down-the-phoenix-suns-spain), et en profite pour remercier [Antoine Tartrou](https://twitter.com/Tartrou) pour les liens !
Coté Monty, ça ne date pas de son arrivée aux Suns. Il l'utilisait déjà à la Nouvelle-Orléans, en mettant en place des backscreens, puisque c'est de ça dont il s'agit, après un classique P&R. Juste de quoi foutre un bordel monstre dans la défense adverse en créant non pas une mais différentes options pour le porteur de ballon en sortie d'écran. La saison passée, c'est une des armes clefs de l'équipe de l'Arizona offensivement. Voici quelques exemples en bonne et due forme :

- d'abord ici :

![Spain PR](https://rallythevalley.fr/assets/images/Spain1.gif)

- puis là avec un détail clef, Booker ultra dispo pour filocher du parking de l'église mormone du coin :

![Spain PR](https://rallythevalley.fr/assets/images/Spain2.gif)

Ces deux actions sont explicites, le trio Ayton-Paul-Booker a tout pour être infernal la saison prochaine sur la base du Spain pick & roll. Rien de surprenant, c'est le scénario qu'on voit à Phoenix depuis la mise en place du duo CP3/Monty aux manettes tactiques de l'équipe. Pour autant, on peut clairement dire que cette troisième saison consécutive doit marquer un step-up en terme d'utilisation d'Ayton en tant que menace offensive. Quitte à marquer un recul pour Paul au scoring ?


#### Booker porteur de balle

Point Book is real. Booker est un excellent porteur de balle. Il représente un danger offensif en iso tel que lorsqu'il est balle en main, la défense adverse ne peut pas se permettre une seule seconde de flottement, sans quoi c'est pull-up à mi-distance entre deux défenseurs. Mais Point Book c'est aussi une grosse vista des familles. Une vision du jeu qui le rend capable de choisir un autre finisseur avec altruisme et efficacité. Mikal, Biyombo, Ayton et Jae ne s'en sont jamais plaint, chaque fois que Book a pris ses responsabilidads. Cas d'école ici, avec Biyombo, sur un simple P&R avec bounce-pass en grande aisance :

![Point Book](https://rallythevalley.fr/assets/images/pointbook.gif)

Dans la partie précédente de l'article, je pose la question du recul de Paul au scoring. Mais en vrai, la question se pose peut-être même plus globalement. Avec la nécessité de voir Ayton exploser en attaque, et la capacité de Booker à animer le jeu et porter la balle, on peut imaginer cette troisième saison comme une transition où Chris Paul laisserait davantage la gonfle à son collègue de backcourt. Un scénario où il deviendrait à 38ans la troisième option offensive d'une équipe qui vise les sommets. Une position dans l'équipe où on s'appuie sur son adresse au shoot mid-range et également à 3pts. Une évolution qui ferait sens tactiquement, d'autant qu'à tout moment, avec l'intelligence et l'altruisme de Devin Booker, Paul peut reprendre le jeu à son compte en milieu d'action, ou par séquences.

Mais Booker, c'est pas juste de l'iso ou de la vision / distribution de jeu. C'est aussi des transitions agressives. Et ça tombe bien, la transition, c'est le troisième aspect que j'imagine pour le futur playbook des Suns l'an prochain.


#### Transition game

La transition, le jeu rapide, c'est aussi l'héritage de cette franchise. La faculté à foncer juste après un rebond ou une interception, et slasher / pop up / fusiller à 3pts alors que la défense essaie de s'organiser. Booker est à 67 de percentile sur ce type d'action l'an dernier. Ca signifie qu'il est meilleur en transition que 67% des autres joueurs de la NBA. C'est tout à fait honnête, on va pas se mentir. Mais vous savez quoi ? Oui, c'est ça, c'est même pas le meilleur de l'équipe. A vrai dire, il se fait même démonter par son pote du poste 3, le petit chouchou de la fanbase des Suns, le bien aimé bien nommé Mikal Bridges.

Mikal, on le connait pour sa redoutable défense, élite à n'en pas douter, qui lui a valu une belle course au trophée de DPOY, et un All-NBA defensive team cette saison. On parle pas de Michel qui défend le week-end parce qu'il sait pas attaquer. Non, on parle d'un étau en titane capable de défendre sur les meilleurs ailiers, arrières et 4 légers de la league, et peser fort sur un game. Mais si c'était que ça, bon bah ce serait cool déjà hein, mais en fait non. Mikal Bridges c'est 89 de percentile en transition. Le mec est une arme de pointe en contre-attaque. Monty a donc tout à gagner à appuyer sur cette capacité de Mikal à exceller en contre-attaque, comme les Suns l'ont fait avec Shawn Marion à l'époque.

Le playbook ne variera sans doute pas des masses l'an prochain pour les Suns, en tout cas si l'effectif n'est pas chamboulé de A à Z par l'arrivée de Kevin Durant et le départ de certains de nos joueurs. Mais ce playbook peut être affiné et updaté, notamment autour du rôle que jouera Chris Paul tant sur la priorisation des shoots d'Ayton pzr rapport aux siens, mais aussi par une balle orange qui pourrait davantage être portée et distribuée par Booker. Je suis convaincu que c'est en rendant la présence de Chris Paul moins obligatoire dans le jeu des Suns que celui-ci trouvera un rôle d'autant plus dangereux et important. On verra ce que nous réserve Monty à la rentrée !
