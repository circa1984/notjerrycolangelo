---
layout: post
title:  "Bogdanovic est-il la recrue idéale des Suns ?"
author: gregoire
categories: [ Team, Roster ]
tags: [ Bojan, Bogdanovic, Roster ]
image: 'assets/images/8.png'
---

C'est dans les tuyaux de rumeurs depuis quelques semaines déjà. Bojan Bogdanovic pourrait être sur les tablettes de notre franchise ensoleillée comme recrue cet été. En provenance du Jazz, chez qui Phoenix enverrait évidemment quelques assets, Bojan Bogdanovic, grand 3 ou 4 au large est vu comme une des recrues permettant aux Suns de passer un cap. Il serait aussi vu comme un potentiel remplacant idouane si Crowder confirmait ses envies d'ailleurs et trouvait un accord susceptible de l'envoyer dans une autre franchise, d'une façon ou d'une autre.
Bojan a la réputation d'un jour intelligent, capable balle en main, shooter soigné. Réputation gagnée en silence et en discretion, mais pas nécessairement usurpée.
Néanmoins, offrerait-il aux Suns les armes dont l'équipe a besoin avec cette nouvelle saison qui arrive ?

#### Jean Claude Stabilitey
Le profil mental du joueur du Jazz colle à la mentalité de Phoenix, avec cette recherche de joueurs de complément qui possèdent un QI basket important, et qui sont habiles. 
Dans le roster actuel, on pense à Saric. Dans l'épopée des années 2000, on se souvient de Diaw. Est-ce que ca colle avec les besoins de Monty sur le terrain de ce point de vue là ? Oui.

Maintenant l'intelligence ne suffit pas. Il faut aussi un rendement, un impact sur le score à la fin du game, des statistiques, de la production. Je sais, ça sonne atrocement capitaliste (pléonasme), mais c'est la NBA. Et si nous devons bouger un effectif qui sort de deux saisons de haute-volée, il faut que ca fasse sens de ce point de vue là.

Bojan statistiquement c'est ça :

![Bojan saison 2022](https://rallythevalley.fr/assets/images/Bojan2022.png)

Une grosse stabilité depuis son arrivée dans les montagnes mormones. A peu près 18 puntos par soirée, une adresse très très honnête qu'on peut lire dans ses pourcentages de loin, sur la ligne et totaux, bref, de la propreté et de la stabilité. Certes, ca évolue un poil à la baisse ces deux dernières années, mais rien d'énorme.
C'est grand, gênant mais pas lock down en défense, ca s'insère dans un collectif comme une Smart dans une place camion. En bref, c'est une recrue qui ressemble aux Suns de James Jones : maline, bien vue, stable.

#### Quel genre de trade ?

Il serait chelou de lacher un Cam Johson pour choper la dernière année de contrat de Bojan Bogdanovic... On peut donc plutôt imaginer voir Crowder aller faire sa dernière année de contrat au Jazz (voire même arriver à choper un buy-out si le Jazz tanke, et se retrouver chez un contender pour les play-offs, et c'est tout ce qu'on lui souhaite !), accompagné peut-être de Shamet pour équilibrer l'aspect thunes de l'affaire.

Tout à fait possible en revanche de voir Danny Ainge nous demander quelques seconds tours par ci par là, en fourbe, ou plutôt en manager avisé et précis qu'il est.
Ca nous donnerait donc un trade du genre (sans les picks) :

![Bojan trade 2022](https://rallythevalley.fr/assets/images/Bojantrade.png)

C'est un trade qui fait sens, indéniablement, pour tout le monde.

#### Bojan dans le roster

Je l'ai dit un peu plus haut, grâce à son QI basket, l'intégration d'un joueur comme Bojan dans le playbook de Monty, c'est pas la mer à boire pour le Coach of the Year 2022. Ses atouts sont nombreux et collent aux systèmes développés autour de Booker et de Paul, et mêmes ceux qu'on peut espérer voir fleurir autour d'Ayton.

Bojan sera-t'il titulaire ? La question peut se poser. On récupère un an de contrat d'un joueur au rendement certain et stable. Il pourrait très bien occuper ce poste 4 laissé vacant par un Jae Crowder qui a fait le chemin inverse, à coté de Mikal en 3. Et ça a de la gueule.

Mais on pourrait aussi l'imaginer en sortie de banc. Avec un Cam Johnson qui hausse son niveau et sa production, voir le jeune soleil rejoindre le 5 et prendre du galon, c'est pas l'idée la plus con qu'on aurait dans l'Arizona. Ce serait peut-être très tôt, certes, mais il y a des franchises qui ont tenté ce genre de trucs en balancant des jeunes joueurs tôt dans le 5, et qui ont été relativement contentes du résultat (Tony Parker aux Spurs, ca parle à quelqu'un ?). Bojan se retrouverait avec un rôle de leader offensif de la second unit. Cam Payne aurait pour devoir de le trouver et de lui créer des occasions de rentabiliser son adresse et sa qualité de shoot.

Bref, les solutions ne manquent pas, et c'est un luxe de pouvoir imaginer que ce trade intéresse Jones, et excitant de lire que les Suns seraient en train de négocier sa venue. Faisons donc confiance au maestro James Jones, capable d'attendre le moment opportun pour recruter un renfort de choix, de ce calibre, au meilleur tarif.
Et vous, Bojan, dans le 5 ou 6ème homme ?