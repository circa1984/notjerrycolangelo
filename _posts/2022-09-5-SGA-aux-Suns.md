---
layout: post
title:  "Shaï aux Suns ?"
author: gregoire
categories: [ Team, Roster ]
tags: [ Shai, SGA, Booker, Paul ]
image: 'assets/images/7.png'
---

Alors qu'on a vécu un été calme en terme de trades coté Suns, une rumeur agite en ce moment même les commentateurs de la franchise de l'autre coté de l'Atlantique. Une rumeur aussi intéressante qu'improbable, ou en tout cas qui pouvait être stupéfiante jusqu'à la blessure récente du néo-drafté Chet Holmgren du coté de l'Oklahoma.
Cette rumeur enverrait un package arizonien du coté du Thunder pour récupérer ce fabuleux guard qu'est Shaï Gildeous-Alexander.
Alors SGA aux Suns, on en dit quoi ?

#### Jean Claude Précipation

D'abord, il peut paraitre logique d'imaginer que le Thunder a perdu quelques uns de ses plus gros espoirs collectifs avec la blessure de son tout jeune joueur drafté très haut. On attendait en effet de cette équipe qu'elle vise autre chose que le tanking pour rentabiliser au max tous les picks accumulés par Sam Presti depuis qu'il est en charge des affaires là-bas. On était en droit d'imaginer que Shaï et ses potes allaient pouvoir enfin jouer les troubles fêtes à l'Ouest, en allant titiller un play-in, par exemple. De quoi prendre de l'expérience, de quoi remettre la franchise sur la carte des équipes devant lesquelles on se présente préparé et prêt à en découdre. Et clairement, la blessure d'Holmgren vient fatiguer ces plans là.

A ce jour, on voit à nouveau le Thunder tanker, et on voit cette franchise rempiler pour sa stratégie habituelle. Mais la question qui nous intéresse aujourd'hui est la suivante : est-ce qu'on voit Shaï supporter une énième saison à voir son équipe se faire piler le crâne chaque soir pendant 82 matches ? Nous avions vécu le même scénario avec Book, et les questions à l'époque ne manquaient pas de fuser non plus.

Pour autant, n'est-il pas précipité d'imaginer Shaï Gildeous-Alexander, jeune guard de très haut niveau, potentiel incroyable, être tradé dès cette rentrée ?
Si, clairement. Enfin, perso, je vois pas Shams débouler en balle demain matin sur Twitter et taper "_the Thunder and the Suns agreed on the trade of SGA. The guard will join the Suns roster while Phoenix sends every draft pick the franchise will have until 2087_". Nope.

En revanche, un trade entre Décembre et Février ? Probable, peut-être. Et pour me confier entièrement, j'espère. Je trouverais même ça très très cool, du moment que nous parvenons à conserver Book, Mikal et DA.


#### SGA chez les Suns, quel type impact ?

SGA la saison dernière, c'est du très gros :

![SGA saison 2022](https://rallythevalley.fr/assets/images/SGA2022.png)

On est sur un guard de niveau all-star (qui pour le nier ?), fort avec le ballon, en pénétration, en provocation, avec des pourcentages très corrects près du cercle, mid range et sur la ligne des lancers francs.

Sa taille est également un avantage majeur. On voit combien Chris Paul se fait malmener face à des match-ups physiquement plus imposants et costauds (Jrue Holiday en 2021, on fait pas le dessin...).

Sa marge de progression est encore grande. La défense, les tirs du parking, SGA a de quoi devenir un vrai gros problème majeur s'il développe ne serait-ce qu'un peu ces deux aspects du jeu. Encore plus s'il se retrouve dans une équipe au collectif bien rodé et huilé comme celui des Suns depuis deux saisons.

Son impact direct est évident : de la création, du scoring, et l'effacement d'une faiblesse névralgique dans notre backcourt (là où Shamet, Payne et Paul collectionnent les petits formats physiques). Alors oui, SGA, si tu viens aux Suns, on sera ravis mon vieux. RA-putain de VIS.

#### Le Trade lui même

Je ne crois donc pas à un trade dès demain (j'espère que cette affirmation ne vieillira pas soudainement mdr). Mais j'imagine les équipes discuter d'un potentiel accord autour de Décembre, ou avant le ASG. Sam Presti va nous demander de lacher nos picks jusqu'à une date infinie genre la prochaine bague des Knicks, mais ça colle avec la mentalité _win-now_ du front-office des Suns.

Les assets s'organiseraient autour de cette longue liste de picks, autour desquels on caleraient des joueurs capables de faire la balance économique. Là dessus, je ne crois pas, et j'espère vraiment de tout mon corazon, que nous aurons besoin de lâcher des éléments aussi balaises que les joueurs cités dans les rumeurs qui concernaient Kevin Durant plus tôt cet été.
Mikal Bridges et Deandre Ayton devraient donc rester dans l'Arizona. En revanche, Cam Johnson pourrait rejoindre l'Oklahoma ainsi que quelques role players.
Voici donc un premier scénario autour de cette idée :

![SGA trade 1 vers les Suns](https://rallythevalley.fr/assets/images/SGAtrade1.png)

Les Suns enverraient Crowder, Shamet et Cam Johnson au Thunder, sans compter les inombrables tours de draft évidemment, contre Shaï.

Mais un autre trade me parait intéressant, c'est le suivant :

![SGA trade 2 vers les Suns, incluant Chris Paul](https://rallythevalley.fr/assets/images/SGAtrade2.png)

Est-ce que j'envoie (ou plutôt renvoie) Chris Paul dans l'Oklahoma sans pression ni vergogne ? La réponse est oui. Est-ce qu'il s'agit de mon seum post série versus les Mavs en playoffs cette année ? La réponse est non (promis). Mais _business is business_.

Paul est un meneur all-time, un hall of famer qui nous a apporté ce dont nous avions besoin pour progresser, prendre en confiance et permettre à coach Monty Williams d'avoir une influence sur le collectif encore plus grande. Mais est-il le futur de notre franchise ? Non. Est-on certain qu'il sera décisif dans les moments forts qu'on veut vivre en fin de saison cette année encore ? Toujours non. Est-il capable de changer de mindset quand la pression du titre se fait de plus en plus grande ? Difficile à prévoir.

Autrement dit, CP3 peut partir, si c'est pour SGA, ...

Dans tous les cas, ce trade est loin d'être une idée idiote. Même si _in fine_, on va tous jouer à Start/Start/Bench avec Book, SGA et CP3 (sauf donc si ce dernier est envoyé dans l'Oklahoma). D'ailleurs, vous, vous startez qui et vous benchez qui ?