---
layout: post
title:  "Qu'attendre de Deandre Ayton, Monty Williams et Chris Paul la saison prochaine"
author: gregoire
categories: [ Team ]
tags: [ Ayton, Paul, Monty ]
image: 'assets/images/5.png'
---

Il y a quelques jours, [les Phoenix Suns matchaient l'offre de contrat des Indiana Pacers pour Deandre Ayton](https://rallythevalley.fr/3/). Le jeune et prometteur pivot retourne donc à la casa et sera dans la raquette des Suns la saison prochaine, indiscutable titulaire du système de Monty Williams.
Contrat fraichement signé en présence de Robert Sarver et James Jones, les boss de la franchise de l'Arizona, Deandre Ayton réintègre donc l'équipe qui l'a drafté 1er en 2018, le collectif avec qui il aura connu les finales face aux Bucks en 2021, le groupe qui a connu un sordide et amer destin lors des derniers playoffs en date.

Face à cette fresque de détails et de petites choses qui font l'équilibre d'une team, sa capacité à rebondir, revenir et gagner de nouveau, on est en droit de se questionner sur ce que sera cette saison pour le trio formé par Ayton, Monty et CP3. Chris Paul sera-t'il enfin determinant dans l'explosion tant attendue de DA ? Monty et Ayton ont-ils remis à plat les choses après cette série face aux Mavs ?

#### Jean Claude Pactole

On va pas se mentir, la promesse de grosse thune que Deandre Ayton va encaisser sur son RIB La Caisse d'Epargne Phoenix a dû soigner quelques plaies. Des plaies qui restaient ouvertes pour lui : avoir son contrat max parce qu'il voulait faire partir de ce clan de joueurs max, marquer de façon générationnelle un changement pour ses proches, son clan, sa famille... La question du mérite ne se posaient pas vraiment en analysant un ensemble de paramètres allant de sa production réelle à son potentiel, en passant par sa valeur sur le marché au regard des autres profils à son poste ou la renégociation des droits TV qui vont tomber d'ici deux ou trois ans et encore faire exploser les contrats max...

Ayton revient donc à la barraque le coeur soigné. Apaisé sans doute de savoir où son avenir va se jouer. Parce qu'il s'agit bien d'un contrat qui couvrira une période décisive pour lui : le pré-prime. Son prime devant commencer à la fin du contrat qu'il vient d'émarger avec les Suns, les 4 prochaines années seront cruciales pour tout le monde et surtout pour DA lui-même s'il veut continuer à pouvoir revendiquer un contrat max et voir des équipes le lui proposer comme les Pacers l'ont très intelligement fait.

Mais est-ce que l'argent suffit dans la situation des Suns post playoffs 2022 ? Est-ce que dans la configuration émotionnelle et collective actuelle, ce nouveau contrat marque vraiment un nouvel élan, un nouveau départ ? Sans se mentir, j'en veux à Monty Williams et Chris Paul pour leur gestion de Deandre Ayton pendant cette série face aux Mavs. Et notamment ce dernier game où rien ne justifiait, au regard du contexte si important au delà des egos, que DA soit soudé au banc comme il le fut, et sorti des systèmes de jeu. Une réconciliation devait avoir lieu pour que ce contrat ne soit plus qu'un détail dans l'histoire des Suns et de leur été 2022.

#### Calin collectif ?

Dans [une récente interview du pivot avec ESPN](https://www.espn.com/nba/story/_/id/34264981/deandre-ayton-happy-resolution-signing-extension-phoenix-suns-tout-vital-piece), Deandre confit que sa relation avec coach Monty Williams est calme. De son coté, le coach a indiqué que le jour de l'explosion face aux Mavs était une anomalie, un "mauvais jour", un de ces jours où rien ne va (et on va pas le contredire hein ahah). Il ajoute également que l'absence de problèmes contractuels la saison prochaine devrait assainir la situation. On peut croire à ces trois affirmations non ?

On ressent une sorte de soulagement général. Un calme après une grosse tempête. Ayton, comme Monty et Jones, semble communiquer une sérénité maintenant que ce contrat est signé et que chacun connait son rôle et son degré d'implication pour les 4 prochaines années.  C'est sans doute une réalité. Après tout, une jeune de 23ans qui assure son avenir et l'avenir de ses proches, son implication dans une même ville, pour a minima les 4 prochaines années, on peut imaginer que c'est rassurant pour lui. Et que si lui est assuré-rassuré, alors son management, à la fois du coté du coaching staff comme dans les bureaux de la franchise, doit également souffler et penser plus sereinement les dossiers qui restent sur la table (plan de jeu pour les uns, trades / free agents pour les autres / Kevin Durant pour tout un peuple).

La grande inconnue pour moi reste Chris Paul. Un meneur de (sale ?) caractère, qui ne traine pas que des dossiers vierges quant à la façon dont un vestiaire peut évoluer avec lui au cours d'une saison. Chris Paul est à ce titre imprévisible et ses interviews de camps d'entrainement et de début de saison seront peut-être de bons indicateurs de son humeur et de son mind-set.

CP3 a la balle en main. Il est le dépositaire des systèmes d'un coach qu'il respecte et qu'il aime fraternellement. Il est la clef principale de l'intégration d'Ayton dans le collectif des Suns sur le terrain. Il est même la clef principale du développement (ou même de l'explosion) de Deandre Ayton la saison prochaine. Il a fait de D.Jordan un all-star, il y a clairement de quoi faire avec Ayton hein... Sa relation avec le pivot est donc précieuse, et déterminante pour l'équipe. La façon dont ces deux-là vont se chercher, se trouver, s'entendre, communiquer, peut faire une grosse différence. Pour Ayton, pour Monty, et surtout pour les résultats de l'équipe et de la franchise. Espérons donc qu'elle soit saine, bonne, productive.

Au milieu de ce triangle, Devin Booker. Le franchise player indiscutable, qui devra montrer que le boss, c'est lui. Sans parler de scoring ou de terrain nécessairement d'ailleurs. Dans le vestiaire. L'équipe doit s'aligner sur ses curseurs (très élevés) de motivations et de determination, et les egos doivent être rester à la maison. Une tâche pas facile, mais confiance en Devin on doit avoir.

Je ne boude pas mon plaisir : revoir Deandre, Mikal et Book ensemble la saison prochaine et pour encore un petit bout de temps si tout roule, c'est du très bon. Très hâte.
